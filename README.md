# Jira api-client

## Installation
```shell
if [ -z command -v python ]; then
  sudo apt-get -yqq update apt-get install -yqq python3.9
fi
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.9
python setup.py
```

## Usage
```shell
jira <ticket_number> <time_in_minutes> <'kebab-cased-log-message'>
```
