import subprocess
import getpass
import os

subprocess.run(['pip', 'install', 'pytz', 'requests'])
SCRIPT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'src/client.py')
sudo_password = getpass.getpass(prompt='sudo password: ')
p = subprocess.Popen(['sudo', 'ln', '-sn', SCRIPT_PATH, '/usr/local/bin/jira'],
                     stderr=subprocess.PIPE, stdout=subprocess.PIPE,  stdin=subprocess.PIPE)

try:
    out, err = p.communicate(input=(sudo_password+'\n').encode(), timeout=5)

except subprocess.TimeoutExpired:
    p.kill()


if os.path.isfile('.env'):
    print('Jira installed successfully, try running jira')
else:
    print('Make sure to visit https://id.atlassian.com/manage-profile/security/api-tokens to create an api token '
          'which you should then put in a file called `.env` at the root of this project, '
          'preceded by a line containing you email address')
