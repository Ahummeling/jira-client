#!/usr/bin/env python
import json
import os
import sys

import requests
from datetime import datetime
import pytz

with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../.env')) as file:
    EMAIL_ADDRESS = file.readline().strip()
    API_KEY = file.readline().strip()

tz = pytz.timezone('Europe/Amsterdam')
BASE_URI = 'https://adivare.atlassian.net/rest/api/2'


def get_now() -> str:
    dt = datetime.now(tz)
    fmt = "%Y-%m-%dT%H:%M:%S%z"
    return dt.strftime(fmt)


def format_json(comment, time, dt) -> dict:
    return {
        "comment": comment,
        "timeSpentSeconds": str(time),
        "created": dt
    }


def log_ticket(tid, t, comment, dt=get_now()):
    uri = f'{BASE_URI}/issue/{tid}/worklog'
    json_data = json.loads(json.dumps(format_json(comment, t*60, dt)))
    response: requests.Response = requests.post(
        uri, json=json_data, allow_redirects=False, auth=(EMAIL_ADDRESS, API_KEY))
    print(f'status: {response.status_code}')
    print(f'response text: {response.text}')
    with open('response_log_ticket.log', 'w') as response_file:
        response_data = response.json()
        if response_data:
            json.dump(response.json(), response_file, indent=4)


arg_tid = None
arg_t = None
arg_comment = None
try:
    arg_tid = sys.argv[1]
    arg_t = sys.argv[2]
    arg_comment = sys.argv[3]
except IndexError:
    print(f'Usage: `jira <ticket_number> <time_in_minutes> <comment>')
    print(f'Parsed input as: jira {arg_tid} {arg_t} {arg_comment}')
    exit(1)
log_ticket(arg_tid, int(arg_t), arg_comment)
